(function ($, Drupal, window, document, undefined) {

    "use strict";

    Drupal.behaviors.breakPoints = {
        attach: function (context, settings) {

            // Verify that the user agent understands media queries.
            if (!window.matchMedia('only screen').matches) {
                return;
            }

            // Get the teatheme breakpoints
            // created in teatheme.breakpoints.yml
            // and added as settings in teatheme.theme function teatheme_preprocess_html
            var bp = settings['teatheme']['breakpoints'];

            // Dynamically add the current breakpoint as a body class via the enquire.js library.
            function registerEnquire(breakpoint_label, breakpoint_query) {
                enquire.register(breakpoint_query, {
                    match: function () {
                        document.body.classList.add('tea--' + breakpoint_label);
                    },
                    unmatch: function () {
                        document.body.classList.remove('tea--' + breakpoint_label);
                    }
                });
            }

            // Register the breakpoints.
            for (var item in bp) {
                if (bp.hasOwnProperty(item)) {
                    registerEnquire(item, bp[item]['mediaquery']);
                }
            }

        }
    };

    Drupal.behaviors.radioFix = {
        attach: function (context, settings){
            $('input[type="radio"]').change(function() {
                $(this).closest('.form-radios').find('input[type="radio"]').removeAttr('checked');
                $(this).attr('checked', 'checked');
            });
        }
    };

})(jQuery, Drupal, this, this.document);


