(function ($, Drupal, window, document, undefined) {

  "use strict";

  Drupal.behaviors.nestedMenu = {
    attach: function (context, settings) {
      // Show L1 menu items on desktop and widesreen
      $('body.tea--desktop, body.tea--widescreen').find('nav.block-menu > ul.menu-root')
          .siblings('.menu-toggle')
          .addClass('menu-shown')
          .html('<span class="visually-hidden">hide this menu</span>');
    }
  };

})(jQuery, Drupal, this, this.document);
