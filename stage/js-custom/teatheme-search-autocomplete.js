(function ($, Drupal, window, document, undefined) {

  "use strict";

  /**
   * Attaches the autocomplete behavior to all required fields.
   */
  Drupal.behaviors.teathemeAutocomplete = {
    attach: function (context) {

      $('body').on('DOMNodeInserted', 'ul.ui-autocomplete', function () {
        console.log('now');

        // Append the autocomplete results to the form.
        var formId = $(':focus').closest('form').attr('id');
        $(this).appendTo((formId) ? formId : 'body');

      });

    }
  };

})(jQuery, Drupal, this, this.document);
