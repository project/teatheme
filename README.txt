
ABOUT TEATHEME
--------------

TeaTheme is a subtheme of the D8 core Stable theme.
TeaTheme uses Gulp to:
  compile sass to css:
    via npm gulp-sass which is a very light-weight wrapper around node-sass,
    which is a Node binding for libsass, which is a port of Sass:
      https://www.npmjs.com/package/gulp-sass
  and compile custom js to minified js:
    with jscs checking:
      https://www.npmjs.com/package/gulp-jscs
      https://github.com/fluxsauce/jscs-drupal
TeaTheme Gulp compiles:
  from /source (editable files) to /stage (expanded files)
    allowing inspection of the /stage files
  and then from /stage (expanded files) to /serve (compressed files)
    allowing control of aggregation for the /serve files
  Note: contrib js files are copied from /source to /serve to /stage unchanged.
D8 uses libraries to:
  conditionally include twig templates and their css and js files,
  allowing css and js to only be served when required.
    TeaTheme therefore provides the base.css file (served on every page)
    plus multiple library-based css files (only served when required).

ABOUT STABLE
------------

Stable is the default base theme; it provides minimal markup and very few
CSS classes. If you prefer more structured markup see the Classy base theme.

Stable allows core markup and styling to evolve by functioning as a backwards
compatibility layer for themes against changes to core markup and CSS. If you
browse Stable's contents, you will find copies of all the Twig templates and
CSS files provided by core.

Stable will be used as the base theme if no base theme is set in a theme's
.info.yml file. To opt out of Stable you can set the base theme to false in
your theme's .info.yml file (see the warning below before doing this):
  base theme: false

Warning: Themes that opt out of using Stable as a base theme will need
continuous maintenance as core changes, so only opt out if you are prepared to
keep track of those changes and how they affect your theme.

ABOUT DRUPAL THEMING
--------------------

For more information, see Drupal.org's theming guide.
https://www.drupal.org/theme-guide/8
